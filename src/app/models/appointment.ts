import {BaseModel} from "./base-model";

export class Appointment implements BaseModel  {
  public id?: string;
  public dayName?: string;
  public isDeleted?: boolean = false;
  public isBooked?: boolean = false;
  public time?: Date;
  public date?: Date;
}
