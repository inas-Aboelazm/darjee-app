﻿import {BaseModel} from "./base-model";

export class User implements BaseModel  {
    id: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    token: string;
    avatar:string;
    isVerified:boolean = false;
}