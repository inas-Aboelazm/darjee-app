import {BaseModel} from "./base-model";

export class ContactUs implements BaseModel  {
  public id?: string;
  public name?: string;
  public isDeleted?: boolean = false;
  public content?: string;
  public email?: string;
  public mobile?: string;
}
