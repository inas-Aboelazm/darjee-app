import {BaseModel} from "./base-model";

export class Booking implements BaseModel  {
  public id?: string;
  public name?: string;
  public email?: string;
  public mobile?: string;
  public duration?: any = {};
  public isDeleted?: boolean = false;
  public date?: Date;
  
}
