import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { BookingsService } from '../services/bookings.service';
import { HttpClient } from '@angular/common/http';
import { Router, NavigationExtras } from '@angular/router';
import { AppointmentsService } from '../services/appointments.service';
import { Appointment } from '../models/appointment';
import { Booking } from '../models/booking';
import * as _ from 'lodash';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {
  @ViewChild('successModal') successModal: TemplateRef<any>;
  appointments: Appointment[] = [];
  satApps: Appointment[] = [];
  sunApps: Appointment[] = [];
  monApps: Appointment[] = [];
  tueApps: Appointment[] = [];
  wedApps: Appointment[] = [];
  thuApps: Appointment[] = [];
  friApps: Appointment[] = [];
  booking: any = {};
  dateNotSelected: boolean = true;
  customizeBooking: boolean = false;
  navigationExtras: NavigationExtras;
  successModalRef: BsModalRef;
  constructor(
    public bookingsService: BookingsService, 
    private router: Router,
    public http: HttpClient, 
    public appointmentsService: AppointmentsService,
    private modalService: BsModalService
    ) {
  }

  ngOnInit() {
    this.createAppointments()
  }

  createAppointments() {
    this.truncateAppointments();
    this.appointmentsService.query({order: 'time ASC'}).subscribe(res=> {
      this.appointments = res as Appointment[];
        this.appointments.forEach(app => {
          switch(app.dayName) {
            case 'saterday': {this.satApps.push(app); break;}; 
            case 'sunday':   {this.sunApps.push(app); break;}; 
            case 'monday':   {this.monApps.push(app); break;}; 
            case 'tuesday':  {this.tueApps.push(app); break;}; 
            case 'wednesday':{this.wedApps.push(app); break;}; 
            case 'thursday': {this.thuApps.push(app); break;}; 
            case 'friday':   {this.friApps.push(app); break;}; 
          }
        })
        this.satApps = _.orderBy(this.friApps, ['time'], ['asc'])
        this.sunApps = _.orderBy(this.sunApps, ['time'], ['asc'])
        this.monApps = _.orderBy(this.monApps, ['time'], ['asc'])
        this.tueApps = _.orderBy(this.tueApps, ['time'], ['asc'])
        this.wedApps = _.orderBy(this.wedApps, ['time'], ['asc'])
        this.thuApps = _.orderBy(this.thuApps, ['time'], ['asc'])
        this.friApps = _.orderBy(this.friApps, ['time'], ['asc'])

    }) 
  }

  dateSelected(date: any) {
    this.booking.date = date;
    this.dateNotSelected = false;
  }
  openHome() {
    this.router.navigate(['/home']).then();
  }

  arrayLoop(n: number): any[] {
    const array = [];
    for (let i = 1; i < n; ++i) {
      array.push(i);
    }
    return array;
  }

  openSuccessModal(template: TemplateRef<any>) {
    this.successModalRef = this.modalService.show(template);
  }

  onSubmit(mForm: any) {
    if(mForm.valid && !this.dateNotSelected){
      this.bookingsService.save(this.booking).subscribe(res => {
        let bookedApp = this.appointments.filter(app => app.time === this.booking.date)[0];
        bookedApp.isBooked = true;
        this.appointmentsService.save(bookedApp).subscribe(res2 => {
          this.createAppointments();
          mForm.reset()
          mForm.submitted = false
          this.dateNotSelected = true;
          this.openHome();
          // this.openSuccessModal(this.successModal);
          // setTimeout(() => {
          //   this.successModalRef.hide();
          //   this.openHome()
          // }, 3000);
        })
      });
      if(this.customizeBooking) {
        this.navigationExtras = {
          queryParams: this.booking
        }
        this.router.navigate(['/customize-booking', this.booking]).then()
      }
    }
  }

  truncateAppointments() {
    this.appointments = [];
    this.satApps = [];
    this.sunApps = [];
    this.monApps = [];
    this.tueApps = [];
    this.wedApps = [];
    this.thuApps = [];
    this.friApps = [];
  }

}
