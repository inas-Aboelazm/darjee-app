import { Component, OnInit } from '@angular/core';
import { ContactUs } from '../models/contactUs';
import { ContactUsService } from '../services/contactUsService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  contactUs: ContactUs = {
    name: '',
    mobile: '',
    email: '',
    content: '',
  }
  constructor(public contactUsService: ContactUsService,private router: Router) { }

  ngOnInit() {
  }

  onSubmit(mForm: any) {
    if(mForm.valid) {
      this.contactUsService.save(this.contactUs).subscribe(()=>{
        this.router.navigate(['/home']).then();
        mForm.reset();
        mForm.submitted = false
      });
    }
  }
}
