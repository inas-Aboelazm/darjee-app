import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  //styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {
  isVerified:boolean=false;
  constructor(
    private usersApi:UserService,
    private route :ActivatedRoute,
    private router:Router
    ) { }

  ngOnInit() {
  }

  verify(){
    console.log(this.route.snapshot.params.id)
    let userId=this.route.snapshot.params.id
    this.usersApi.confirmEmail(userId).subscribe((res: any)=>{
      console.log('res--->', res)
      if(!!res.isVerified){
        this.isVerified=true;
        //this.router.navigate(['/login'])
      }
    })
  }
}
