import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Booking } from '../models/booking';

@Component({
  selector: 'app-customize-booking',
  templateUrl: './customize-booking.component.html',
  styleUrls: ['./customize-booking.component.scss']
})
export class CustomizeBookingComponent implements OnInit {
  booking: Booking;
  constructor(public route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(res => {
      this.booking = {
        name: res['name'],
        date: res['date']
      }
    })
  }

}
