import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomizeBookingComponent } from './customize-booking.component';

describe('CustomizeBookingComponent', () => {
  let component: CustomizeBookingComponent;
  let fixture: ComponentFixture<CustomizeBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomizeBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomizeBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
