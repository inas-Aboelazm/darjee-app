import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from './helpers/auth.guards';

import { BookingComponent } from './booking/booking.component';
import { LoginComponent } from 'src/app/login/login.component';
import { HomeComponent } from 'src/app/home/home.component';
import { RegisterComponent } from 'src/app/register/register.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CustomizeBookingComponent } from './customize-booking/customize-booking.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { VerifyComponent } from 'src/app/verify/verify.component';

const routes: Routes = [
  {path: 'booking', component: BookingComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'contactUs', component: ContactUsComponent},
  {path: 'home', component: HomeComponent},
  {path: '', component: HomeComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'customize-booking', component: CustomizeBookingComponent, canActivate: [AuthGuard]},
  {path: 'checkout', component: CheckoutComponent, canActivate: [AuthGuard]},
  {path: 'verified/:id', component: VerifyComponent},
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
