import { Component, OnInit } from '@angular/core';
import {AuthenticationService } from '../services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  //styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
  ) { 
    
  }

  ngOnInit() {
  }
  logOut(){
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
