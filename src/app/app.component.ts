import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './services/authentication.service';
import { User } from './models/user';

@Component({ 
  selector: 'app-root', 
  templateUrl: 'app.component.html' 
})
export class AppComponent implements OnInit{
    currentUser: User;
    userId:string;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
    }

    ngOnInit() {
        if(!!this.authenticationService.currentUserValue){
            let user: any=this.authenticationService.currentUserValue
            this.userId= user.userId;
            console.log('value---->' , this.userId)
        }
    }

    logOut() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }
}