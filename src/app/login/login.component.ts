import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService , SocialUser } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider, LinkedInLoginProvider } from "angularx-social-login";

import {AuthenticationService } from '../services/authentication.service';
import { AlertService } from '../services/alert.service';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  emailNotVerified: boolean = false;
  userId: string;
  private user: User;
  private loggedIn: boolean;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
private socialUser:{}
  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService,
      private alertService: AlertService,
      private authService: AuthService,
      private usersService: UserService
  ) {
      //redirect to home if already logged in
      if (this.authenticationService.currentUserValue) { 
          this.router.navigate(['/home']);
      }
  }
  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
      // social login
      this.authService.authState.subscribe((user) => {
        this.loggedIn = (user != null);
        if (user && user.authToken) {
            this.socialUser = {
                  token:  user.authToken,
                  email:  user.email,
                  firstName:  user.firstName,
                  lastName:  user.lastName,
                  avatar:  user.photoUrl,
                  password: ""
              };
              console.log("user--social->", this.socialUser)
             // store user details and jwt token in local storage to keep user logged in between page refreshes
             localStorage.setItem('currentUser', JSON.stringify(this.socialUser));
             this.router.navigate(['/home']);
             //this.currentUserSubject.next(user);
         }
      });
  }
  signOut(): void {
    this.authService.signOut();
  }
  ngOnInit() {
      this.loginForm = this.formBuilder.group({
          email: ['', Validators.required],
          password: ['', Validators.required]
      });

      // get return url from route parameters or default to '/'
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';

  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  alert(message: string) {
    window.alert(message);
  }
  async onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }

      this.loading = true;
      let data = await this.authenticationService.login(this.f.email.value, this.f.password.value).toPromise();
      if(data['notVerified']) {
        this.emailNotVerified = true;
        this.userId = data.userId
      } else {
        this.emailNotVerified = false;
        this.alertService.success("Success");
        this.router.navigate([this.returnUrl]);    
    }              
  }

  resendEmail() {
      console.log("clicked")
      this.usersService.resendEmail(this.userId).subscribe();
  }
}
