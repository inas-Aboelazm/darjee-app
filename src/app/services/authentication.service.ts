﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../models/user';
import { UserService } from './user.service';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    public token: String;
    constructor(private http: HttpClient, public usersService: UserService) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(email: string, password: string) {
        return this.http.post<any>(`${environment.baseUrl}/users/login`, { email, password })
            .pipe(map(async (user) => {
                // login successful if there's a jwt token in the response
                if (user && user.id) {
                    this.token= user.token;
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    let userInfo = await this.usersService.get(user.userId).toPromise();
                        if(userInfo.isVerified) {
                            localStorage.setItem('currentUser', JSON.stringify(user));
                            this.currentUserSubject.next(user);        
                        } else {
                            user.notVerified = true;
                        }
                        return user;
                }
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}