import {HttpClient, HttpHeaders} from "@angular/common/http";

import {BaseModel} from "./../models/base-model";
import {environment} from "../../environments/environment";

export class BaseApiService<T extends BaseModel> {
  public baseUrl = environment.baseUrl;
  public url = "/";
  protected http: HttpClient;

  constructor(private httpClient: HttpClient) {
    this.http = this.httpClient;
  }

 /* protected authorization(token_1?: any): HttpHeaders {
    let token = (JSON.parse(localStorage.getItem('token'))) ?
      (JSON.parse(localStorage.getItem('token'))).id : null;
    if (token_1 && !token) {
      token = token_1;
    }
    let currentCountry = (JSON.parse(localStorage.getItem('currentCountry'))) ?
      (JSON.parse(localStorage.getItem('currentCountry'))).id : null;

    if (token) {
      if (currentCountry)
        return new HttpHeaders()
          .set("Content-Type", "application/json")
          .set("Accept", "application/json")
          .set("country_id", currentCountry.toString())
          .set('Authorization', token);
      else
        return new HttpHeaders()
          .set("Content-Type", "application/json")
          .set("Accept", "application/json")
          .set('Authorization', token);
    } else {
      if (currentCountry)
        return new HttpHeaders()
          .set("Content-Type", "application/json")
          .set("Accept", "application/json")
          .set("country_id", currentCountry.toString());
      else
        return new HttpHeaders()
          .set("Content-Type", "application/json")
          .set("Accept", "application/json");
    }

  }*/

  list() {
    return this.http
      .get<T>(this.baseUrl + this.url)
  }

  get(id: string) {
    return this.http
      .get<T>(this.baseUrl + this.url + `/${id}`)
  }

  query(query: any) {
    return this.http
      .get<T[]>(this.baseUrl + this.url + '?filter=' + `${JSON.stringify(query)}`)
  }

  save(item: T) {
    return item.id ? this.update(item) : this.add(item);
  }

  add(item: T) {
    return this.http.post<T>(this.baseUrl + this.url, item)
  }

  update(item: T) {
    return this.http.patch<T>(this.baseUrl + this.url + `/${item.id}`, item)
  }

  destroy(id: String): any {
    return this.http
      .delete<T>(this.baseUrl + this.url + `/${id}`)

  }
}
