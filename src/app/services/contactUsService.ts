import { Injectable } from '@angular/core';
import {BaseApiService} from "./base-api";
import {HttpClient} from "@angular/common/http";
import { ContactUs } from '../models/contactUs';

@Injectable()
export class ContactUsService extends BaseApiService<ContactUs> {

  constructor(http: HttpClient) {
    super(http);
    this.url = '/contactUs';
  }


}
