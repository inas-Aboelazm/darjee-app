import { Injectable } from '@angular/core';
import {BaseApiService} from "./base-api";
import {HttpClient} from "@angular/common/http";
import { Appointment } from '../models/appointment';

@Injectable()
export class AppointmentsService extends BaseApiService<Appointment> {

  constructor(http: HttpClient) {
    super(http);
    this.url = '/appointments';
  }


}
