﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { BaseApiService } from './base-api';
import { environment } from 'src/environments/environment';

@Injectable()
export class UserService extends BaseApiService<User> {
    constructor(http: HttpClient) {
        super(http);
        this.url = '/users';
    }
    register(user: User) {
        return this.http.post(`${environment.baseUrl}/users`, user);
    }
    resendEmail(id: string) {
        console.log(`${this.url}/resendEmail?id=${id}`)
        return this.http.get(`${environment.baseUrl}${this.url}/resendEmail?id=${id}`);
    }
    confirmEmail(id: string) {
        console.log(`${environment.baseUrl}${this.url}/confirmEmail?id=${id}`)
        return this.http.get(`${environment.baseUrl}${this.url}/confirmEmail?id=${id}`);
    }

}