import { Injectable } from '@angular/core';
import {BaseApiService} from "./base-api";
import {HttpClient} from "@angular/common/http";
import {Booking} from "../models/booking";

@Injectable()
export class BookingsService extends BaseApiService<Booking> {

  constructor(http: HttpClient) {
    super(http);
    this.url = '/bookings';
  }


}
