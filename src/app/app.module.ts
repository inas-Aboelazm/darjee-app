import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { FacebookLoginProvider} from "angularx-social-login";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor} from './helpers/error.interceptor';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { NgxPayPalModule } from 'ngx-paypal';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { BookingComponent } from './booking/booking.component';
import { LoginComponent } from 'src/app/login/login.component';
import { HomeComponent } from 'src/app/home/home.component';
import { HeaderComponent } from 'src/app/header/header.component';
import { NavComponent } from 'src/app/nav/nav.component';
import { RegisterComponent } from './register/register.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CustomizeBookingComponent } from './customize-booking/customize-booking.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { VerifyComponent } from './verify/verify.component';
import { AlertComponent } from './alert/alert.component';

import { UserService } from 'src/app/services/user.service';
import { BookingsService } from './services/bookings.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AlertService } from 'src/app/services/alert.service';
import { AppointmentsService } from './services/appointments.service';
import { ContactUsService } from './services/contactUsService';




let config = new AuthServiceConfig([
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("Facebook-App-Id")
  }
]);

const routes: Routes = [
  {path: 'booking', component: BookingComponent},
  {path:'login', component: LoginComponent},
  {path:'contactUs', component: ContactUsComponent},
  {path:'', component: LoginComponent},
  {path:'home', component: HomeComponent},
  {path: 'register', component: RegisterComponent}

];

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    BookingComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    NavComponent,
    RegisterComponent,
    ContactUsComponent,
    CustomizeBookingComponent,
    CheckoutComponent,
    VerifyComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SocialLoginModule,
    NgxPayPalModule,
    RouterModule.forRoot(routes),
    CarouselModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [
    BookingsService,
    UserService,
    AuthenticationService,
    AlertService,
    AppointmentsService,
    ContactUsService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
